pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

rootProject.name = "Electrolux_Assignment"
include(":androidApp")
include(":shared")
package com.assignment.electroluxassignment.android.di

import com.assignment.electroluxassignment.android.features.photolist.PhotoListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModules = module {

    viewModel { PhotoListViewModel(get()) }
}
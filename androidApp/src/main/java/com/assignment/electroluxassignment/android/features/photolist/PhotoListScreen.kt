package com.assignment.electroluxassignment.android.features.photolist


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemsIndexed
import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkInfo
import androidx.work.WorkManager
import coil.compose.rememberImagePainter
import com.assignment.electroluxassignment.android.MainActivity
import com.assignment.electroluxassignment.android.R
import com.assignment.electroluxassignment.android.util.ImageDownloadManager
import com.assignment.electroluxassignment.domain.model.Photo
import kotlinx.coroutines.flow.Flow

import org.koin.androidx.compose.getViewModel


@Composable
fun PhotoListScreen() {

    val viewModel = getViewModel<PhotoListViewModel>()
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = stringResource(id = R.string.title_photos)) }
            )
        }
    ) { padding ->

        val context = LocalContext.current
        val selectedLocation = remember { mutableStateOf<ActivityResult?>(null) }
        var photoUrl by remember { mutableStateOf("") }
        val launcher =
            rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                selectedLocation.value = it
            }

        downloadImage(selectedLocation, photoUrl, context = context)

        PhotoPagingList(viewModel.photos) { imageUrl, id ->

            // Action on onLongPress
            photoUrl = imageUrl
            val mimeType = getMimeType(imageUrl)
            val photoName = "$id.jpg"

            if (mimeType?.isNotEmpty() == true) {
                val intent = Intent()
                intent.action = Intent.ACTION_CREATE_DOCUMENT
                val mimeTypes: Array<String> = arrayOf(mimeType)
                intent.type = mimeType
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
                intent.putExtra(Intent.EXTRA_TITLE, photoName)
                launcher.launch(
                    Intent.createChooser(
                        intent,
                        ""
                    )
                )
            } else Toast.makeText(context, R.string.err_photo_not_available, Toast.LENGTH_LONG).show()
        }
    }
}

@Composable
fun PhotoPagingList(
    photosPagingFlow: Flow<PagingData<Photo>>, onLongPress: (String, String) -> Unit
) {

    val photoItems = photosPagingFlow.collectAsLazyPagingItems()
    var selectedIdx by remember { mutableStateOf(-1) }

    val borderDefault = BorderStroke(0.dp, Color.Transparent)
    val borderSelected = BorderStroke(2.dp, Color.Green)


    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(20.dp)
    ) {
        itemsIndexed(photoItems) { index, photo ->
            photo?.let {
                Card(
                    modifier = Modifier
                        .padding(10.dp)
                        .height(200.dp)
                        .fillMaxWidth()
                        .pointerInput(Unit) {
                            detectTapGestures(onLongPress = {
                                selectedIdx = if (selectedIdx != index) index else -1
                                onLongPress(photo.imageUrl, photo.id)
                            }, onTap = {
                                selectedIdx = if (selectedIdx != index) index else -1
                            })
                        },
                    shape = RoundedCornerShape(12.dp),
                    elevation = 12.dp,
                    border = if (selectedIdx == index)
                        borderSelected else borderDefault
                ) {
                    val painter = rememberImagePainter(data = photo.imageUrl, builder = {
                        error(R.drawable.ic_outline_broken_image_24)
                    })

                    Image(
                        painter = painter,
                        contentDescription = null,
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(200.dp)
                            .padding(8.dp)
                            .clip(shape = RoundedCornerShape(8.dp)),
                        contentScale = ContentScale.Crop
                    )
                }

            }
        }
    }

    photoItems.apply {
        when {
            loadState.refresh is LoadState.Loading -> {
                Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxSize()) {
                    CircularProgressIndicator()
                }
            }

            loadState.refresh is LoadState.Error -> {
                Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxSize()) {
                    Text(
                        text = stringResource(id = R.string.err_unable_to_load_photo),
                        modifier = Modifier.padding(12.dp),
                        textAlign = TextAlign.Center
                    )
                }
            }
        }
    }

}
private fun downloadImage(
    downloadLocation: MutableState<ActivityResult?>,
    url: String,
    context: Context
) {
    if (downloadLocation.value != null && downloadLocation.value?.resultCode == Activity.RESULT_OK) {

        val uri = downloadLocation.value?.data?.data
        if (uri != null) {
            Toast.makeText(context, "Downloading...", Toast.LENGTH_SHORT).show()
            val builder = Data.Builder()
            builder.putString("downloadLocation", uri.toString())
            builder.putString("url", url)
            val inputParams = builder.build()
            val downloadFileWorker = OneTimeWorkRequest.Builder(ImageDownloadManager::class.java)
                .setInputData(inputParams)
                .build()
            val workManager = WorkManager.getInstance(context)
            workManager.enqueue(downloadFileWorker)
            val outputWorkInfo: LiveData<WorkInfo> =
                workManager.getWorkInfoByIdLiveData(downloadFileWorker.id)
            outputWorkInfo.observe(context as MainActivity, Observer {
                if (it.state == WorkInfo.State.SUCCEEDED) {
                    Toast.makeText(context, R.string.lbl_photo_downloaded, Toast.LENGTH_LONG).show()
                } else if (it.state == WorkInfo.State.FAILED) {
                    Toast.makeText(context, R.string.err_download_failed, Toast.LENGTH_LONG).show()
                }
            })

        }
    }
}

private fun getMimeType(url: String): String? {
    var type: String? = null
    try {
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        }
    } catch (e: Exception) {

    }
    return type
}


package com.assignment.electroluxassignment.android

import android.app.Application
import com.assignment.electroluxassignment.android.di.viewModelModules
import com.assignment.electroluxassignment.di.initKoin
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.logger.Level

class ElectroluxApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        initKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(this@ElectroluxApplication)
            modules(
                viewModelModules
            )
        }
    }
}
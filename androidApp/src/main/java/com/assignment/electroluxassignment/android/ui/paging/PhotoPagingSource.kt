package com.assignment.electroluxassignment.android.ui.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import coil.network.HttpException
import com.assignment.electroluxassignment.domain.interactors.GetPhotosUseCase
import com.assignment.electroluxassignment.domain.model.Photo
import com.assignment.electroluxassignment.util.Config.PAGE_SIZE
import java.io.IOException

class PhotoPagingSource(private val getPhotosUseCase: GetPhotosUseCase) :
    PagingSource<Int, Photo>() {

    override fun getRefreshKey(state: PagingState<Int, Photo>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Photo> {
        return try {
            val nextPage = params.key ?: 1
            val photoList = getPhotosUseCase(nextPage, PAGE_SIZE)
            LoadResult.Page(
                data = photoList,
                prevKey = if (nextPage == 1) null else nextPage - 1,
                nextKey = if (photoList.isEmpty()) null else nextPage + 1
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }
}
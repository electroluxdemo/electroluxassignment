package com.assignment.electroluxassignment.android.features.photolist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.assignment.electroluxassignment.android.ui.paging.PhotoPagingSource
import com.assignment.electroluxassignment.domain.interactors.GetPhotosUseCase
import com.assignment.electroluxassignment.domain.model.Photo
import com.assignment.electroluxassignment.util.Config.PAGE_SIZE
import com.assignment.electroluxassignment.util.Config.PREFETCH_PAGE_SIZE
import kotlinx.coroutines.flow.*

class PhotoListViewModel(private val getPhotosUseCase: GetPhotosUseCase) : ViewModel() {

    val photos: Flow<PagingData<Photo>> = Pager(PagingConfig(pageSize = PAGE_SIZE*2, prefetchDistance = PREFETCH_PAGE_SIZE, enablePlaceholders = false)) {
        PhotoPagingSource(getPhotosUseCase)
    }.flow.cachedIn(viewModelScope)

}
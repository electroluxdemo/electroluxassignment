import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
}



android {

    signingConfigs {
        getByName("debug") {
            storeFile = file("../keystore.jks")
            storePassword = "electro1ux@assignment"
            keyAlias = "Electrolux"
            keyPassword = "electro1ux@assignment"
        }
    }
    compileSdk = Versions.compileSdkVersion
    defaultConfig {
        applicationId = "com.assignment.electroluxassignment.android"
        minSdk = Versions.minSdkVersion
        targetSdk = Versions.targetSdkVersion
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
        getByName("debug") {
            signingConfig = signingConfigs.getByName("debug")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.0.5"
    }
}

dependencies {
    implementation(project(":shared"))

    implementation(Libraries.composeActivity)
    implementation(Libraries.composeUI)
    implementation(Libraries.composeMaterial)
    implementation(Libraries.composeTooling)
    implementation(Libraries.composeMaterialIcons)
    implementation(Libraries.composePaging)
    implementation(Libraries.composeNav)
    implementation(Libraries.composeCoil)
    implementation(Libraries.koin)
    implementation(Libraries.koinCompose)
    implementation (Libraries.worker_runtime)
}
package com.assignment.electroluxassignment.util

object Config {


    const val BASE_URL = "https://api.flickr.com/services/rest"
    const val API_KEY = "1c96bf7167eb886bff741df66af3e3a3"
    const val METHOD = "flickr.photos.search"
    const val TAGS = "Electrolux"
    const val EXTRAS = "url_m, description"
    const val PAGE_SIZE = 100
    const val PREFETCH_PAGE_SIZE = 50
    const val FORMAT = "json"
    const val NO_JSON_CALL_BACK = "true"

}
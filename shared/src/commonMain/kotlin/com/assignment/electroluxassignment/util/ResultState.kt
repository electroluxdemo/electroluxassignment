package com.assignment.electroluxassignment.util

sealed class ResultState<out T> {
    object default : ResultState<Nothing>()
    object loading : ResultState<Nothing>()
    class Success<out T> (val data: T): ResultState<T>()
    class Error(val message: String): ResultState<Nothing>()
}

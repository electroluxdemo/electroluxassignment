package com.assignment.electroluxassignment.domain.interactors

import com.assignment.electroluxassignment.domain.repository.PhotoRepo

class GetPhotosUseCase(private val photoRepo: PhotoRepo) {

    operator suspend fun invoke(pageNo: Int, pageSize: Int) = photoRepo.getPhotosFromRemotePaging(pageNo, pageSize)
}
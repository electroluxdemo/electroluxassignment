package com.assignment.electroluxassignment.domain.repository

import com.assignment.electroluxassignment.domain.model.Photo
import com.assignment.electroluxassignment.util.ResultState
import kotlinx.coroutines.flow.Flow

interface PhotoRepo {
    suspend fun getPhotosFromRemotePaging(pageNo: Int, pageSize: Int): List<Photo>
}
package com.assignment.electroluxassignment.domain.model

data class Photo(
    val id: String,
    val title: String,
    val description: String,
    val imageUrl: String
)
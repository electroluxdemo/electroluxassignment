package com.assignment.electroluxassignment.data.datasource.remote

import com.assignment.electroluxassignment.data.datasource.remote.response.PhotoApiResponse
import com.assignment.electroluxassignment.util.Config.EXTRAS
import com.assignment.electroluxassignment.util.Config.METHOD
import com.assignment.electroluxassignment.util.Config.TAGS
import io.ktor.client.HttpClient
import io.ktor.client.request.*
import io.ktor.http.*


class PhotoApi(private val endPoint: String, private val httpClient: HttpClient) {

    suspend fun getPhotos(pageNo: Int, pageSize: Int): PhotoApiResponse {
        return httpClient.get {
            apiUrl(pageNo, pageSize)
        }
    }

    private fun HttpRequestBuilder.apiUrl(pageNo: Int, pageSize: Int) {
        url {
            takeFrom(endPoint)
            parameter("method", METHOD)
            parameter("tags", TAGS)
            parameter("extras", EXTRAS)
            parameter("per_page", pageSize)
            parameter("page", pageNo)
        }
    }
}
package com.assignment.electroluxassignment.data.datasource.remote.response

import com.assignment.electroluxassignment.data.datasource.remote.response.dto.PhotoDto
import kotlinx.serialization.Serializable


@Serializable
data class PhotoApiResponse(
    val photos: PhotoResponse
)

@Serializable
data class PhotoResponse(
    val page: Int,
    val photo: List<PhotoDto>
)

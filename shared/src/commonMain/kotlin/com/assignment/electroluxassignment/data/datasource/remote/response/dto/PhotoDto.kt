package com.assignment.electroluxassignment.data.datasource.remote.response.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PhotoDto(
    @SerialName("id")
    val id: String,
    @SerialName("title")
    val title: String? = null,
    @SerialName("description")
    val description: Description? = null,
    @SerialName("url_m")
    val imageUrl: String? = null
)

@Serializable
data class Description(
    @SerialName("_content")
    val content: String? = null
)
package com.assignment.electroluxassignment.data.repository

import com.assignment.electroluxassignment.data.datasource.remote.PhotoApi
import com.assignment.electroluxassignment.data.mapper.PhotoMapper
import com.assignment.electroluxassignment.domain.model.Photo
import com.assignment.electroluxassignment.domain.repository.PhotoRepo
import com.assignment.electroluxassignment.util.Config
import com.assignment.electroluxassignment.util.ResultState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class PhotoRepoImpl(private val photoApi: PhotoApi, private val photoMapper: PhotoMapper) :
    PhotoRepo {

    override suspend fun getPhotosFromRemotePaging(pageNo: Int, pageSize: Int): List<Photo> {
        val results = photoApi.getPhotos(pageNo, pageSize)
        return results.photos.photo.map { photoMapper.map(it) }
    }
}
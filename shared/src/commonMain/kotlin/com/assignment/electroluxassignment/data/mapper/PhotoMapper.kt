package com.assignment.electroluxassignment.data.mapper

import com.assignment.electroluxassignment.base.Mapper
import com.assignment.electroluxassignment.data.datasource.remote.response.dto.PhotoDto
import com.assignment.electroluxassignment.domain.model.Photo

class PhotoMapper : Mapper<PhotoDto, Photo>() {

    override fun map(model: PhotoDto) = Photo(
        id = model.id,
        title = model.title.orEmpty(),
        description = model.description?.content.orEmpty(),
        imageUrl = model.imageUrl.orEmpty()
    )
}
package com.assignment.electroluxassignment.di

import com.assignment.electroluxassignment.data.datasource.remote.PhotoApi
import com.assignment.electroluxassignment.data.mapper.PhotoMapper
import com.assignment.electroluxassignment.data.repository.PhotoRepoImpl
import com.assignment.electroluxassignment.domain.interactors.GetPhotosUseCase
import com.assignment.electroluxassignment.domain.repository.PhotoRepo
import com.assignment.electroluxassignment.util.Config.API_KEY
import com.assignment.electroluxassignment.util.Config.BASE_URL
import com.assignment.electroluxassignment.util.Config.FORMAT
import com.assignment.electroluxassignment.util.Config.NO_JSON_CALL_BACK
import io.ktor.client.HttpClient
import io.ktor.client.features.*
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import kotlinx.coroutines.Dispatchers
import kotlinx.serialization.json.Json
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.dsl.KoinAppDeclaration
import org.koin.dsl.module

fun initKoin(appDeclaration: KoinAppDeclaration) =
    startKoin {
        appDeclaration()
        modules(
            repositoryModule,
            mapperModule,
            useCasesModule,
            platformModules()
        )
    }

// IOS
fun initKoin() = initKoin {}

val repositoryModule = module {
    single<PhotoRepo> { PhotoRepoImpl(get(), get()) }
    single { PhotoApi(get(), get()) }

    single {
        HttpClient {

            install(JsonFeature) {
                serializer = KotlinxSerializer(
                    json = Json {
                        ignoreUnknownKeys = true
                    }
                )
            }


            install(Logging) {
                logger = Logger.DEFAULT
                level = LogLevel.INFO
            }

            install(DefaultRequest) {
                parameter("api_key", API_KEY)
                parameter("format", FORMAT)
                parameter("nojsoncallback", NO_JSON_CALL_BACK)
            }

        }
    }

    single { BASE_URL }
}

val useCasesModule: Module = module {
    factory { GetPhotosUseCase(get()) }
}

val mapperModule = module {
    factory { PhotoMapper() }
}



package com.assignment.electroluxassignment

class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform}!"
    }
}
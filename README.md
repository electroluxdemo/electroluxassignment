Electrolux Coding Assignment
=============================

# App Design and language details:
* KMM
* Language: Kotlin
* Dependency Injection: Koin
* Async Handling: Coroutine
* Design Pattern: MVVM + Clean Architecture
* Network: Ktor

# Features:
* List of images using "Electrolux" tag from flicker api.
* Endless scrolling using pagination.
* Tapping on a image will highlight the item on list.
* Press and hold on a photo to download.
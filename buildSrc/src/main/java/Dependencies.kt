object Versions {
    val compileSdkVersion = 31
    val targetSdkVersion = 31
    val minSdkVersion = 21

    val kotlinxSerializationJson = "1.3.2"
    val kotlin_version = "1.5.31"
    val timberkt_version = "1.5.1"

    val koin_version = "3.1.2"

    val coroutine_version = "1.3.9"
    val compose_version = "1.0.5"
    val compose_activity = "1.4.0"
    val compose_paging = "1.0.0-alpha14"
    val compose_nav = "2.4.0-rc01"
    val compose_coil = "1.3.2"
    val ktor_version = "1.6.3"
    val ktor_serialization = "1.6.3"
    val logback_classic = "1.2.10"
    val worker_runtime = "2.5.0"

    // Testing dependencies
    val mockito_version = "2.28.2"
    val mockito_ui_version = "2.24.5"
    val coroutines_test_version = "1.4.2"
    val arch_test_version = "2.1.0"
    val expresso_version = "3.4.0"
    val fragment_test_version = "1.3.6"
    val test_rule_version = "1.1.1"
    val ext_junit_version = "1.1.3"
}

object Libraries {

    val koin = "io.insert-koin:koin-android:${Versions.koin_version}"
    val koinCore = "io.insert-koin:koin-core:${Versions.koin_version}"
    val koinCompose = "io.insert-koin:koin-androidx-compose:${Versions.koin_version}"


    // Compose
    val composeUI = "androidx.compose.ui:ui:${Versions.compose_version}"
    val composeActivity = "androidx.activity:activity-compose:${Versions.compose_activity}"
    val composeMaterial = "androidx.compose.material:material:${Versions.compose_version}"
    val composeTooling = "androidx.compose.ui:ui-tooling:${Versions.compose_version}"
    val composeMaterialIcons = "androidx.compose.material:material-icons-extended:${Versions.compose_version}"
    val composePaging = "androidx.paging:paging-compose:${Versions.compose_paging}"
    val composeNav = "androidx.navigation:navigation-compose:${Versions.compose_nav}"
    val composeCoil = "io.coil-kt:coil-compose:${Versions.compose_coil}"
    val composeFoundation = "androidx.compose.foundation:foundation:${Versions.compose_version}"

    val coroutineCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutine_version}"

    val ktor_core = "io.ktor:ktor-client-core:${Versions.ktor_version}"
    val ktor_android = "io.ktor:ktor-client-android:${Versions.ktor_version}"
    val ktor_serialization = "io.ktor:ktor-client-serialization:${Versions.ktor_serialization}"
    val ktor_logging = "io.ktor:ktor-client-logging:${Versions.ktor_version}"
    val logback_classic = "ch.qos.logback:logback-classic:${Versions.logback_classic}"
    val kotlinSerialization = "org.jetbrains.kotlin:kotlin-serialization:${Versions.kotlin_version}"
    val kotlinxSerializationJson = "org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.kotlinxSerializationJson}"

    val worker_runtime = "androidx.work:work-runtime-ktx:${Versions.worker_runtime}"

}

object TestLibraries {
    val jUnit  = "junit:junit:4.12"
    val mockitio = "org.mockito:mockito-core:${Versions.mockito_version}"
    val coroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines_test_version}"
    val arch =  "androidx.arch.core:core-testing:${Versions.arch_test_version}"
    val jUnitExt = "androidx.test.ext:junit:${Versions.ext_junit_version}"
    val expresso = "androidx.test.espresso:espresso-core:${Versions.expresso_version}"
}